from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QComboBox, QMessageBox
from get_processes import *

class Ui_custom_settings(object):

    def setupUi(self, custom_settings):
        custom_settings.setObjectName("custom_settings")
        custom_settings.resize(453, 470)
        font = QtGui.QFont()
        font.setFamily("Courier")
        custom_settings.setFont(font)
        self.centralwidget = QtWidgets.QWidget(custom_settings)
        self.centralwidget.setObjectName("centralwidget")
        self.custom_settings_label = QtWidgets.QLabel(self.centralwidget)
        self.custom_settings_label.setGeometry(QtCore.QRect(150, 30, 201, 51))
        font = QtGui.QFont()
        font.setFamily("Palatino Linotype")
        font.setPointSize(18)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.custom_settings_label.setFont(font)
        self.custom_settings_label.setObjectName("custom_settings_label")

        self.select_screen_dropdown = QComboBox(self.centralwidget)
        self.select_screen_dropdown.setGeometry(QtCore.QRect(120, 110, 201, 31))
        available_screen_list = screens
        self.select_screen_dropdown.addItems(available_screen_list)


        #time interval text-box
        self.select_interval_label = QtWidgets.QLabel(self.centralwidget)
        self.select_interval_label.setGeometry(QtCore.QRect(120, 170, 201, 31))
        self.select_interval_label.setObjectName("label")

        self.select_interval = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.select_interval.setGeometry(QtCore.QRect(290, 170, 40, 30))
        self.select_interval.setObjectName("plainTextEdit")

        # text box for number of screens
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(120, 230, 161, 31))
        self.label.setObjectName("label")

        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(290, 230, 40, 30))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox.setGeometry(QtCore.QRect(120, 330, 231, 23))
        self.checkBox.setObjectName("checkBox")

        self.submit_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.submit_pushButton.setGeometry(QtCore.QRect(180, 380, 89, 25))
        font = QtGui.QFont()
        font.setFamily("Courier")
        font.setBold(True)
        font.setWeight(75)
        self.submit_pushButton.setFont(font)
        self.submit_pushButton.setObjectName("submit_pushButton")

        self.go_back_Btn = QtWidgets.QCommandLinkButton(self.centralwidget)
        self.go_back_Btn.setGeometry(QtCore.QRect(400, 390, 41, 31))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(10)
        self.go_back_Btn.setFont(font)
        self.go_back_Btn.setText("")
        self.go_back_Btn.setObjectName("go_back_Btn")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 30, 121, 81))
        self.label_2.setStyleSheet("background-image: url(:/newPrefix/weboCcult.ico)")
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        custom_settings.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(custom_settings)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 453, 22))
        self.menubar.setObjectName("menubar")
        custom_settings.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(custom_settings)
        self.statusbar.setObjectName("statusbar")
        custom_settings.setStatusBar(self.statusbar)

        self.retranslateUi(custom_settings)
        QtCore.QMetaObject.connectSlotsByName(custom_settings)

    def retranslateUi(self, custom_settings):
        _translate = QtCore.QCoreApplication.translate
        custom_settings.setWindowTitle(_translate("custom_settings", "MainWindow"))
        self.custom_settings_label.setText(_translate("custom_settings", "Custom Settings"))
        # self.dropdown1_label.setText(_translate("custom_settings", "select screen dropdown"))
        # self.dropdown2_label.setText(_translate("custom_settings", "set interval dropdown"))
        self.select_interval_label.setText(_translate("custom_settings", "Enter time interval : "))
        self.label.setText(_translate("custom_settings", "Number of Camera : "))
        # self.dropdown3_label.setText(_translate("custom_settings", "crop image dropdown"))
        self.checkBox.setText(_translate("custom_settings", "click for AI processing "))
        self.submit_pushButton.setText(_translate("custom_settings", "Submit"))

#for logo/icon
import ico_weboccult1
import ico_weboccult

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    custom_settings = QtWidgets.QMainWindow()
    ui = Ui_custom_settings()
    ui.setupUi(custom_settings)
    custom_settings.show()
    sys.exit(app.exec_())
