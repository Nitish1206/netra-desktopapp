# from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from main_window import *
# from settings_window_updated import *

from settings_window import *
from settings_options import *
from details_window import *
from default_settings import *
from get_processes import *
import json
'''for screenshot function'''
import threading
from path_to_folder import findCurrentPath
import time
# from take_screenshot import *
from get_screenshot import *
import traceback
import os
from PyQt5.QtCore import QThread, pyqtSignal
# import logger

class message(QThread):
    signal = pyqtSignal()

    def __init__(self, Window):
        super(message, self).__init__()
        self.window = Window

    def run(self):
        self.signal.emit()


class CustomMessageBox(QMessageBox):
    def __init__(self, *__args):
        QMessageBox.__init__(self)
        self.timeout = 0
        self.autoclose = False
        self.currentTime = 0
    def showEvent(self, QShowEvent):
        self.currentTime = 0
        if self.autoclose:
            self.startTimer(1000)
    def timerEvent(self, *args, **kwargs):
        self.currentTime += 1
        if self.currentTime >= self.timeout:
            self.done(0)

    @staticmethod
    def showWithTimeout(timeoutSeconds, message, minimize_flag):
        w = CustomMessageBox()
        w.setStyleSheet("QLabel{min-width:300 px; font-size: 24px;} QPushButton{ width:150px; font-size: 18px; }")
        w.autoclose = False
        # w.timeout = timeoutSeconds
        w.setWindowTitle("Recognition")
        w.setText(message)
        w.setStandardButtons(QMessageBox.Ok)
        retval=w.exec_()
        # current_time = datetime.now().strftime("%H_%M_%S")
        if retval == QMessageBox.Ok:
            minimize_flag = True

        # if retval == QMessageBox.No:
        #     print("No")

        return minimize_flag




class MainAPP(QMainWindow):

    def __init__(self): 
        super().__init__()
        #MainWindow instance
        self.main_window_obj = Ui_MainWindow()
        self.main_window_obj.setupUi(self)
        #Settings_options window jsons
        self.minimize_flag = False
        with open("json_data\custom.json", encoding='utf-8', errors='ignore') as json_data:
            self.custom_json_data = json.load(json_data, strict=False)
        with open("json_data\default.json", encoding='utf-8', errors='ignore') as json_data:
            self.default_json_data = json.load(json_data, strict=False)
        # print(self.default_json_data[0]['selected_screen'])
        #Running process
        self.available_screen_list = screens

        self.running_threads=[]
        self.window_counter=0

        #------Buttons------
        #MainWindow
        self.setWindowTitle("Main Screen")
        self.main_window_obj.start_buttton.clicked.connect(self.open_setting_options_window)
        self.main_window_obj.stop_button.clicked.connect(self.stop_btn_function)
        self.main_window_obj.settings_button.clicked.connect(self.open_settings_window)
        self.main_window_obj.details_button.clicked.connect(self.open_details_window)

        self.minimize_message = message(self)
        self.minimize_message.signal.connect(self.minimze_box)
        self.main_window_obj.error_button.clicked.connect(self.minimize_message.start)

        self.screen_not_available_message = message(self)
        self.screen_not_available_message.signal.connect(self.error_box)
        self.main_window_obj.error_button2.clicked.connect(self.screen_not_available_message.start)

        # self.show()

        # for ScreenShot thread
        # self.thread_name = "thread_name"
        self.capture_flag = False
        self.screen_name = "screen_name"
        self.user_interval = 1
        self.thread_flag = True
        # self.thread_name = threading.Thread(target=self.start_capture)
        # self.thread_name.start()

        # if current_time >= next_5_minutes and update_status is False:
        #     """check new update of build for every 5 minutes"""
        #     logger.info("checking for update")
        #     app_update = pyupdater_client.update_check(APP_NAME, APP_VERSION)
        #     logger.info("update checked successfully")
        #     if app_update is None:
        #         update_status = False
        #     else:
        #         logger.info("update status %s %s", app_update.filename, app_update.version)
        #         logger.info("downloading new build")
        #         threading.Thread(target=download_update, args=(app_update,)).start()
        #         update_status = True
        #     next_5_minutes = datetime.now() + dt.timedelta(minutes=config["update_check_at"])


    def minimze_box(self):
        # QMessageBox.information(self, 'Warning', 'Success', QMessageBox.Ok)
        self.capture_flag = CustomMessageBox.showWithTimeout(5, "screen minimized", self.capture_flag)

    def error_box(self):
        # QMessageBox.information(self, 'Warning', 'Success', QMessageBox.Ok)
        self.capture_flag = CustomMessageBox.showWithTimeout(5, "screen not available", self.capture_flag)

    #-------Methods---------
    # select settings_option window instance
    def open_setting_options_window(self):
        print("Clicked on Start Button")

        self.close()
        select_settings_window = QMainWindow()
        select_settings_obj = Ui_select_settings_window()
        select_settings_obj.setupUi(select_settings_window)
        select_settings_obj.custom_settings_btn.toggled.connect(lambda : self.toggle_btn_function(select_settings_obj))
        select_settings_obj.default_settings_btn.toggled.connect(lambda: self.toggle_btn_function(select_settings_obj))
        select_settings_obj.custom_settings_buttonBox.accepted.connect(lambda: self.accepted_custom(select_settings_window,select_settings_obj))
        select_settings_obj.custom_settings_buttonBox.rejected.connect(lambda: self.rejected_custom(select_settings_window))
        select_settings_obj.default_settings_buttonBox.accepted.connect(lambda: self.accepted_default(select_settings_window, select_settings_obj))
        select_settings_obj.default_settings_buttonBox.rejected.connect(lambda: self.rejected_default(select_settings_window))
        select_settings_obj.running_process_dropdown.addItems(self.available_screen_list)
        with open("json_data\default.json", encoding='utf-8', errors='ignore') as json_data:
            self.default_json_data = json.load(json_data, strict=False)
        for index in range(len(self.default_json_data)):
            screen_name = 'Screen name: ' + str(self.default_json_data[index]['selected_screen'])
            interval = 'Time Interval: ' + str(self.default_json_data[index]['selected_interval']) + " " + 'seconds'
            select_settings_obj.default_settings_textBrowser.append(screen_name)
            select_settings_obj.default_settings_textBrowser.append(interval)
        select_settings_window.show()

    # Settings_window Instance
    def open_settings_window(self):
        print("Clicked on settings Button")

        self.close()
        window_settings = QMainWindow()
        window_settings.settings_window_obj = Ui_settings_window()
        window_settings.settings_window_obj.setupUi(window_settings)
        window_settings.settings_window_obj.general_radio_btn.toggled.connect(lambda: self.settings_toggle_btn_function(window_settings.settings_window_obj))
        window_settings.settings_window_obj.screenshot_btn.toggled.connect(lambda: self.settings_toggle_btn_function(window_settings.settings_window_obj))
        window_settings.settings_window_obj.back_btn.clicked.connect(lambda: self.back_from_settings(window_settings))
        window_settings.settings_window_obj.run_application_startup_btn.clicked.connect(self.open_setting_options_window)
        window_settings.settings_window_obj.default_btn.clicked.connect(self.open_default_settings)
        window_settings.show()

    # Settings_window functions
    def settings_toggle_btn_function(self, settings_window_obj):
        '''settings window - method for radio buttons'''
        if settings_window_obj.general_radio_btn.isChecked():
            print("General checked!")
            settings_window_obj.general_btn_layout_widget.show()
            settings_window_obj.screenshot_btn_layout_widget.hide()
        elif settings_window_obj.screenshot_btn.isChecked():
            print("Screenshot checked!")
            settings_window_obj.screenshot_btn_layout_widget.show()
            settings_window_obj.general_btn_layout_widget.hide()

    def back_from_settings(self, window_settings):
        '''go back from settings to Mainwindow'''
        print("back Clicked!")
        window_settings.close()
        self.show()

    def open_default_settings(self):
        self.close()
        default_settings_window = QMainWindow()
        default_settings_window.default_settings_obj = Ui_default_settings()
        default_settings_window.default_settings_obj.setupUi(default_settings_window)
        default_settings_window.default_settings_obj.running_process_dropdown_2.addItems(self.available_screen_list)
        default_settings_window.default_settings_obj.default_settings_buttonBox.accepted.connect(lambda: self.accepted_default_settings(default_settings_window, default_settings_window.default_settings_obj))
        default_settings_window.show()

    def accepted_default_settings(self, default_settings_window, default_settings_obj):
        '''this function takes input from default window and saves to default.json file'''
        default_data_dict = {
            "selected_screen": default_settings_obj.running_process_dropdown_2.currentText(),
            "selected_interval": default_settings_obj.time_interval_input.text(),
            "AI_checkbox_value": default_settings_obj.ai_processing_checkbox.isChecked(),
            "No_of_cameras": default_settings_obj.no_of_camera_input.text()
        }
        default_data_file = json.load(open('json_data/default.json'))
        if type(default_data_file) is dict:
            default_data_file = [default_data_file]
        default_data_file.append(default_data_dict)
        with open('json_data/default.json', 'w') as outfile:
            json.dump(default_data_file, outfile)
        default_settings_window.close()
        self.show()


    # Details window
    def open_details_window(self):
        print("Opening Details window")

        self.close()
        window_for_details = QMainWindow()
        window_for_details.details_window_obj = Ui_details_window()
        window_for_details.details_window_obj.setupUi(window_for_details)

        # Displaying default data
        title_default = "Default Screen"
        window_for_details.details_window_obj.output_textBrowser.append(title_default)
        window_for_details.details_window_obj.output_textBrowser.append("")
        with open("json_data\default.json", encoding='utf-8', errors='ignore') as json_data:
            self.default_json_data = json.load(json_data, strict=False)
        for index in range(len(self.default_json_data)):
            screen_default = "-> Screen Name: " + str(self.default_json_data[index]['selected_screen'])
            interval_default = "-> Interval: " + str(self.default_json_data[index]['selected_interval'])
            window_for_details.details_window_obj.output_textBrowser.append(screen_default)
            window_for_details.details_window_obj.output_textBrowser.append(interval_default)
        window_for_details.details_window_obj.output_textBrowser.append("")
        # Displaying custom data
        title_custom = "Custom Screen"
        screen_custom = "-> Screen Name: " + str(self.custom_json_data['selected_screen'])
        interval_custom = "-> Interval: " + str(self.custom_json_data['selected_interval'])
        window_for_details.details_window_obj.output_textBrowser.append(title_custom)
        window_for_details.details_window_obj.output_textBrowser.append("")
        window_for_details.details_window_obj.output_textBrowser.append(screen_custom)
        window_for_details.details_window_obj.output_textBrowser.append(interval_custom)
        window_for_details.details_window_obj.back_buton.clicked.connect(lambda:self.back_from_details(window_for_details))
        window_for_details.show()

    def back_from_details(self, window_for_details):
        '''go back from details to Mainwindow'''
        print("back Clicked!")
        window_for_details.close()
        self.show()

    #Mainwindow functions
    def stop_btn_function(self):
        # print("Stop clicked!")
        # self.errorMessage()
        self.stop_capturing_thread()

    #Settings_options functions
    def toggle_btn_function(self, select_settings_obj):
        # print("toggle_btn clicked!")
        if select_settings_obj.default_settings_btn.isChecked():
            # print("default checked!")
            select_settings_obj.default_settings_widget.show()
            select_settings_obj.custom_settings_widget.hide()
        elif select_settings_obj.custom_settings_btn.isChecked():
            # print("Custom checked!")
            select_settings_obj.custom_settings_widget.show()
            select_settings_obj.default_settings_widget.hide()

    def accepted_custom(self, select_settings_window, setting_obj):

        # print("clicked on OK!")
        self.custom_data_dict = {
            "selected_screen": setting_obj.running_process_dropdown.currentText(),
            "selected_interval": setting_obj.interval_input.text()
        }

        select_settings_window.close()
        self.show()
        self.start_thread_for_screenshot(capture_flag=True, screen_name=self.custom_data_dict["selected_screen"], user_interval=self.custom_data_dict["selected_interval"])

    def rejected_custom(self, select_settings_window):
        # print("Clicked on cancel")
        select_settings_window.close()
        self.show()

    def accepted_default(self, select_settings_window, setting_obj):
        select_settings_window.close()
        self.show()
        with open("json_data\default.json", encoding='utf-8', errors='ignore') as json_data:
            self.default_json_data = json.load(json_data, strict=False)
        for index in range(len(self.default_json_data)):
            self.start_thread_for_screenshot(capture_flag=True, screen_name=self.default_json_data[index]["selected_screen"], user_interval=self.default_json_data[index]["selected_interval"])

    def rejected_default(self, select_settings_window):
        print("Clicked on cancel")
        select_settings_window.close()
        self.show()

    def start_thread_for_screenshot(self, capture_flag, screen_name, user_interval):
        # print("inside start_thread_for_screenshot")
        # if not self.thread_flag:
        self.thread_flag = True

        # self.thread_name = thread_name
        self.capture_flag = capture_flag
        self.screen_name = screen_dict[screen_name]
        self.user_interval = int(user_interval)
        self.window_counter+=1
        self.screenshot_thread = threading.Thread(target=self.start_capture, args=(self.screen_name,))
        self.screenshot_thread.start()
        self.running_threads.append(screen_name)
        print(self.running_threads)

    def start_capture(self, screen_name):

        image_dir = findCurrentPath() + os.sep + str(self.window_counter) + "_"
        # print(image_dir)
        if not os.path.isdir(image_dir):
            os.mkdir(image_dir)

        image_path = image_dir

        while self.thread_flag:
            try:
                if self.capture_flag:
                    # print("Started capturing...")
                    returned_value = screenshot(str(int(time.time())), image_path, screen_name)
                    # print("returned_value - ", returned_value)
                    if (returned_value == "Screen Minimized") :
                        self.capture_flag = False
                        self.main_window_obj.error_button.click()

                    elif (returned_value == "Screen is Not available"):
                        self.capture_flag = False
                        self.main_window_obj.error_button2.click()

                    else:
                        time.sleep(self.user_interval)
                else:
                    pass
                    # print("capture flag not set")
            except Exception as e:
                traceback.print_exc()
                sys.exit(0)

    def stop_capturing_thread(self):
        # print("stopped capturing...")
        self.thread_flag=False

    def errorMessage(self):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Warning")
        msg.setText("Your screen is minimized. Please Maximized it")
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        # msg.show()
        x = msg.exec_()

    def warningMessage(self):
        warning = QtWidgets.QMessageBox()
        warning.setWindowTitle('Warning')
        warning.setText("Screen is not available!")
        warning.setIcon(QtWidgets.QMessageBox.Critical)
        # warning.show()
        p = warning.exec_()


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    screen = app.primaryScreen()
    main_app = MainAPP()
    main_app.show()
    sys.exit(app.exec_())
