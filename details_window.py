from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_details_window(object):
    def setupUi(self, details_window):

        details_window.setObjectName("details_window")
        details_window.resize(419, 248)
        self.centralwidget = QtWidgets.QWidget(details_window)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalWidget.setObjectName("verticalWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.title_label = QtWidgets.QLabel(self.verticalWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.title_label.setFont(font)
        self.title_label.setObjectName("title_label")
        self.verticalLayout.addWidget(self.title_label)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem1)
        self.output_textBrowser = QtWidgets.QTextBrowser(self.verticalWidget)
        self.output_textBrowser.setObjectName("output_textBrowser")
        self.verticalLayout.addWidget(self.output_textBrowser)
        self.back_buton = QtWidgets.QCommandLinkButton(self.verticalWidget)
        self.back_buton.setObjectName("back_buton")
        # self.back_buton.clicked.connect(lambda : self.go_to_MainWindow(details_window))
        self.verticalLayout.addWidget(self.back_buton)
        self.gridLayout.addWidget(self.verticalWidget, 0, 0, 1, 1)
        details_window.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(details_window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 419, 21))
        self.menubar.setObjectName("menubar")
        details_window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(details_window)
        self.statusbar.setObjectName("statusbar")
        details_window.setStatusBar(self.statusbar)

        self.retranslateUi(details_window)
        QtCore.QMetaObject.connectSlotsByName(details_window)

    def retranslateUi(self, details_window):
        _translate = QtCore.QCoreApplication.translate
        details_window.setWindowTitle(_translate("details_window", "MainWindow"))
        self.title_label.setText(_translate("details_window", "                         List of screens being captured"))
        self.back_buton.setText(_translate("details_window", "Go back"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    details_window = QtWidgets.QMainWindow()
    ui = Ui_details_window()
    ui.setupUi(details_window)
    details_window.show()
    sys.exit(app.exec_())
