# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['D:\\netra-desktopapp\\main_app.py'],
             pathex=['D:\\netra-desktopapp', 'D:\\netra-desktopapp\\.pyupdater\\spec'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=['c:\\users\\wot-mahek\\.conda\\envs\\cutsomtf\\lib\\site-packages\\pyupdater\\hooks'],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='win',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='win')
