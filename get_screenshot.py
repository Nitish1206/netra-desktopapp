import win32gui
import win32ui
from ctypes import windll
from PIL import Image
# import cv2
import win32con

def screenshot(name, path, screen_from_json):
    # screen_from_json = screen_from_json
    # print(screen_from_json)
    # hWnd = win32gui.FindWindow(None, screen_from_json)  # Window class name can get the SPY ++ using Visual Studio Tools
    try:
        hWnd = screen_from_json # Window class name can get the SPY ++ using Visual Studio Tools
        # Get a handle to the window size information
        # win32gui.ShowWindow(hWnd, 10)
        left, top, right, bot = win32gui.GetWindowRect(hWnd)
    except Exception as e:
        return "Screen is Not available"
    width = right - left
    height = bot - top
    # print("---->>>>>",width,height)
    # Returns the device context handle of the window, covering the entire window, including non-client area, title bars, menus, borders
    '''for screen minimized exception'''
    if width < 400 or height < 100:
        return "Screen Minimized"
    else:
    # win32gui.SetForegroundWindow(hWnd)
        hWndDC = win32gui.GetWindowDC(hWnd)
        # Create a device context
        mfcDC = win32ui.CreateDCFromHandle(hWndDC)
        # Create a memory device context
        saveDC = mfcDC.CreateCompatibleDC()
        # Create a bitmap object ready to save the picture
        saveBitMap = win32ui.CreateBitmap()
        # For the bitmap open space
        saveBitMap.CreateCompatibleBitmap(mfcDC, width, height)
        # Will save the screenshot to the saveBitMap
        saveDC.SelectObject(saveBitMap)
        # Save the bitmap into the memory device context
        result = saveDC.BitBlt((0, 0), (width, height), mfcDC, (0, 0), win32con.SRCCOPY)
        # print("=====>>>>>>>>>>>>>", result)

        # If you want the screenshot to the print device:
        ### final int parameter: 0 - save the entire window, 1 save only the client area. If the function returns a successful PrintWindow
        # result = windll.user32.PrintWindow(hWnd,saveDC.GetSafeHdc(),0)
        # print (result) #PrintWindow successful output 1

        # Save the image
        ## Method One: windows api save
        ### to save the bitmap file
        # saveBitMap.SaveBitmapFile(saveDC, "img_Winapi.bmp")

        ## Method 2 (first part): PIL Save
        ### get the bitmap information
        bmpinfo = saveBitMap.GetInfo()
        bmpstr = saveBitMap.GetBitmapBits(True)
        ### generates an image
        im_PIL = Image.frombuffer('RGB', (bmpinfo['bmWidth'], bmpinfo['bmHeight']), bmpstr, 'raw', 'BGRX', 0, 1)
        ## Method 2 (subsequent revolutions of the second portion)

        ## three methods (first part): opencv + numpy save
        ### get the bitmap information
        signedIntsArray = saveBitMap.GetBitmapBits(True)
        ## The method of tris (subsequent revolutions of the second part)

        # Memory release
        win32gui.DeleteObject(saveBitMap.GetHandle())
        saveDC.DeleteDC()
        mfcDC.DeleteDC()
        win32gui.ReleaseDC(hWnd, hWndDC)

    ## Method 2 (second part): PIL Save
    ### PrintWindow success, saved to a file, to the screen

    if result == None:
        # print(path)
        im_PIL.save(path+"/"+name+".png")  # save
        # im.save(path+"/"+name+".png")
