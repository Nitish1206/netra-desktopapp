import win32gui
import win32ui
from ctypes import windll
from PIL import Image
# import cv2
import win32con

def screenshot(name, path, screen_from_json):
	screen_from_json = screen_from_json
	hwnd = win32gui.FindWindow(None, screen_from_json)
	# print(hwnd)
	# win32gui.SetForegroundWindow(hwnd)

	# Change the line below depending on whether you want the whole window
	# or just the client area.
	# left, top, right, bot = win32gui.GetClientRect(hwnd)
	left, top, right, bot = win32gui.GetWindowRect(hwnd)
	w = right - left
	h = bot - top
	# print(w, h)

	'''for screen minimized exception'''
	if w < 400 or h < 100:
		return "Screen Minimized"
	else:
		hwndDC = win32gui.GetWindowDC(hwnd)
		mfcDC  = win32ui.CreateDCFromHandle(hwndDC)
		saveDC = mfcDC.CreateCompatibleDC()

		saveBitMap = win32ui.CreateBitmap()
		saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)

		saveDC.SelectObject(saveBitMap)

		# Change the line below depending on whether you want the whole window
		# or just the client area.
		#result = windll.user32.PrintWindow(hwnd, saveDC.GetSafeHdc(), 1)
		# result = windll.user32.PrintWindow(hwnd, saveDC.GetSafeHdc(), 0)
		result = saveDC.BitBlt((0,0), (w,h), mfcDC, (0, 0), win32con.SRCCOPY)
		# print("=====>>>>>>>>>>>>>", result)

		bmpinfo = saveBitMap.GetInfo()
		bmpstr = saveBitMap.GetBitmapBits(True)

		im = Image.frombuffer(
			'RGB',
			(bmpinfo['bmWidth'], bmpinfo['bmHeight']),
			bmpstr, 'raw', 'BGRX', 0, 1)

		win32gui.DeleteObject(saveBitMap.GetHandle())
		saveDC.DeleteDC()
		mfcDC.DeleteDC()
		win32gui.ReleaseDC(hwnd, hwndDC)
		if result == None:
			# print(path)
			im.save(path+"/"+name+".png")
