class ClientConfig(object):
    PUBLIC_KEY = 'oku9vsIV4wvf+Viw3GNTjaXOe/2VnIOJQCJD6UsI71Y'
    APP_NAME = 'netra'
    COMPANY_NAME = 'weboccult'
    HTTP_TIMEOUT = 30
    MAX_DOWNLOAD_RETRIES = 3
    UPDATE_URLS = ['https://bitbucket.org/paramwot/netra-desktopapp/src/master/']
