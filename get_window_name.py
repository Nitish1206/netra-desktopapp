#win32gui and pywinauto both can be use for getting running processes

import win32gui
screens = []
screen_dict={}
def winEnumHandler( hwnd, ctx ):
    if win32gui.IsWindowVisible(hwnd):
        # print(hwnd)
        if win32gui.GetWindowText(hwnd) != '':
            if win32gui.GetWindowText(hwnd) not in screens:
                screen_name = win32gui.GetWindowText( hwnd )
                screens.append(win32gui.GetWindowText( hwnd ))
                screen_dict[screen_name]=hwnd
        # print(hex(hwnd), win32gui.GetWindowText( hwnd ))
win32gui.EnumWindows( winEnumHandler, None )
# print(screens)
#
# from pywinauto import Desktop
# # print("PYINAUTO")
# windows = Desktop(backend="uia").windows()
# # print([w.window_text() for w in windows])
# screens = [w.window_text() for w in windows]
# # print(screens)