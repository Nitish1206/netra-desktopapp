import json

def write_in_json(new_dict):
    data = json.load(open('data.json'))

    # convert data to list if not
    if type(data) is dict:
        data = [data]

    # append new item to data lit
    data.append(new_dict)

    # write list to file
    with open('data.json', 'w') as outfile:
        json.dump(data, outfile)

# file = open('data.json', 'r')
# config = json.load(file)
# file.close()
# for i in range(len(config)):
#     print(config[i])